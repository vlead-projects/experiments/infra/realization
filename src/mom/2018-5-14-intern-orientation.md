#+SETUPFILE: ../org-templates/level-1.org

# __Intern Orientation [May 14, 2018]__

## Speaker : Mrityunjay Kumar
**Takeaways :**
- Create content, which helps other people learn
- What aids learning
- What is an experiment? hands on, activity focused leaning process that supplements classroom instruction; supplements not substitute to classroom education
- How do we create an experiment? process of successive refinement of an experiment definition through a series of stages, where each stage produces some artifacts and is driven by roles

```
Pedagogy | teaching style
Taxonomy | defined using some structured language
Pedagogical philosophies | currently following Bloom's taxonomy
    * Bloom  | Remember, Understand, Apply, Analyse, Evaluate, Create
Instructional design strategies | Merill and Gagne
    * Merill | Problem, Activation, Demonstration, Application, Integration
    * Gagne  | Gain attention, Inform learners of objectives, Simulate recall of prior learning, Present the content, Provide learning guidance, Elicit performance, Provide feedback, Assess performance, Enhance retention and transfer
Constructivist theory of learning | "knowledge does not happen if a second person preaches and one listens, it happens when one construct knowledge and himself learns it" - learn by doing !
```

|||
| --- | ---|
|Reading | [Hitchhiker's Guide to the Galaxy](http://izt.ciens.ucv.ve/ecologia/Archivos/Filosofia-I/Adams,%20Douglas%20-%20The%20Hitchhikers%20Guide%20To%20The%20Galaxy.pdf) |

 ### Speaker : Prof. Venkatesh Chopella
**Takeaways :**
- Authoring : writing experiments with prior knowledge using suitable tools
- Importance of sorting : improvement in quantification of ease and efficiency of searching
- Data structures : How you represent things, defines how fast and efficient can you manupulate them
- Lab is supposed to complement classroom teaching - lectures and textbooks, as the modality of working (hands on), provision of interactive medium, and immidiate analysis is different
- Not about programming, let's you start the leaning process to implement learnt concepts in the real world
- Recursion : repetitive unfolding/folding till there's no unfolding/folding left

> Activity : Figuring out recusion (folding/unfolding) for Quicksort

- Fractals : are about growth, the process, transformation; repeating patterns - self similarity

	|||
    | --- | ---|
    |Reading | [How long is the coast of Britain](http://www.statsmapsnpix.com/2016/08/how-long-is-coastline-of-great-britain.html) |
    
> Activity : Time Complexity of Merge sort by counting using pictorial representation

|||
| ------ | ------ |
| Task 1 | Writing experiment in accordance to the grammar and architecture defined |
| Task 2 | Relate the writings to the principles of pedagogy and instructional design |
| Homework 1 |  Find out and document all the resources in one place |
| Readings | Bloom, Merill, Gagne Philosophies |
